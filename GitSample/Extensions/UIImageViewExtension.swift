//
//  UIImageViewExtension.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
extension UIImageView {
    func setImageRoundImage(fromURL imageURLString:String?, placeholderImage:UIImage?) {
        let tempImage:UIImage? = (placeholderImage != nil) ? placeholderImage : nil
        guard let unWrappedImageURLString =  imageURLString, unWrappedImageURLString != " " else {
            self.image = tempImage
            return
        }
        guard let unWrappedURL = URL.init(string: unWrappedImageURLString) else {
            self.image = tempImage
            return
        }
        self.af_setImage(withURL: unWrappedURL,
                         placeholderImage: tempImage,
                         filter:CircleFilter(),
                         imageTransition: UIImageView.ImageTransition.noTransition, completion:
            { (responce) in
                let image  = responce.result.value
                if let image = image {
                    self.image = image
                }
        })
    }
}
