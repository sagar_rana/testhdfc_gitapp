//
//  IndicatableViewExtension.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
import UIKit

extension IndicatableView where Self: BaseViewController {
    
    func showActivityIndicator() {
      self.startActivityIndicator()
    }
    
    func hideActivityIndicator() {
        self.stopActivityIndicator()

    }
    func showAlert(title: String?, message: String?) {
        self.showAlertController(title: title, message:message)
    }
}
