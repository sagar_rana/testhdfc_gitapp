//
//  GenericTitleTableViewCell.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit
import Material
import Motion
class GenericTitleTableViewCell: TableViewCell {
    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblTitle.text = nil
        self.prepareTitleLable()

    }
    func prepareTitleLable(){
        lblTitle.font = RobotoFont.regular(with: 16)
        
    }
    open override func prepare() {
        super.prepare()
         self.dividerColor = Color.grey.lighten2
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTitle(title:String?) {
        if let title = title {
            self.lblTitle?.text = title.firstUppercased
        }
    }
}
