//
//  BranchModel.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
import ObjectMapper

struct BranchModel {
    var name: String?

}

extension BranchModel: Mappable {
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        name   <- map["name"]
   
        
        
    }
}
