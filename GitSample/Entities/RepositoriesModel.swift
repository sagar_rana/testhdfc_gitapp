//
//  RepositoriesModel.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
import ObjectMapper

struct RepositoriesModel {
    var name: String?
    var languages: String?
    var branches: String?
    var contributors: String?
    var fullName: String?
    var owner: OwnerModel?

    
}

extension RepositoriesModel: Mappable {
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        name   <- map["name"]
        languages     <- map["languages_url"]
        branches     <- map["branches_url"]
        contributors     <- map["contributors_url"]
        fullName     <- map["full_name"]
        owner     <- map["owner"]

    }
}
struct OwnerModel {
    var avatarUrl: String?
 
}

extension OwnerModel: Mappable {
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        avatarUrl   <- map["avatar_url"]
    }
}









