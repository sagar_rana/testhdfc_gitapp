//
//  StoryBoardConstants.swift
//  HSPartnerApp
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
import UIKit
enum StoryBoard {
    
    static let LanguagesList: UIStoryboard = UIStoryboard.init(name: "LanguagesListScreens", bundle: nil)
    static let ContributorsList: UIStoryboard = UIStoryboard.init(name: "ContributorsListScreens", bundle: nil)
    static let RepositoriesList: UIStoryboard = UIStoryboard.init(name: "RepositoriesScreen", bundle: nil)
    static let BranchesList: UIStoryboard = UIStoryboard.init(name: "BranchesListScreens", bundle: nil)
     static let GitSampleMain: UIStoryboard = UIStoryboard.init(name: "GitSampleMain", bundle: nil)
    


}



