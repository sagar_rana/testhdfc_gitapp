//
//  SizeEnum.swift
//  GitSample
//
//  Created by hs on 14/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
import UIKit

protocol Size {
   
    
}

enum ViewSizes {
    enum TableViewCell:Size {
        case SingleLineData
        case SingleLineDataWithAvtarImage
        public var estimatedRowHeight:CGFloat {
            switch self {
            case .SingleLineData: return 48
            case .SingleLineDataWithAvtarImage: return 56
            }
        }
    }
}
