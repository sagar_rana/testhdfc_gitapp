//
//  RepositoriesListPresenter.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//


import UIKit

class RepositoriesListPresenter: RepositoriesListPresenterProtocol {

    

    weak private var view: RepositoriesListViewProtocol?
    var interactor: RepositoriesListInteractorInputProtocol!
    private let router: RepositoriesListWireframeProtocol

    init(interface: RepositoriesListViewProtocol, interactor: RepositoriesListInteractorInputProtocol?, router: RepositoriesListWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    func fetchRepositories() {
        self.view?.showActivityIndicator()
        self.interactor.retrieveRepositoriesList()
    }
    func getRepositoriesCount() -> Int {
        return self.interactor.getRepositoriesList().count
    }
    func getRepositoriesData(atIndex:Int) -> RepositoriesModel {
        return self.interactor.getRepositoriesList()[atIndex]
    }
    func showOptions(atIndex:Int)  {
        self.router.navigateToRepositoriesOptionScreen(with: self.getRepositoriesData(atIndex: atIndex))
    }
   
}
extension RepositoriesListPresenter:RepositoriesListInteractorOutputProtocol {
    func onRepositoriesRetrieved() {
        self.view?.hideActivityIndicator()
       self.view?.onRepositoriesRetrieved()
    }
    
    func onError(error: NetworkErrors?) {
        self.view?.hideActivityIndicator()
      self.view?.showAlert(title: ErrorAlerts.Default.DataFetch.title , message: error?.description)
    }
    
    
}
