//
//  RepositoriesOptionPresenter.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class RepositoriesOptionPresenter: RepositoriesOptionPresenterProtocol {
    

    weak private var view: RepositoriesOptionViewProtocol?
    var interactor: RepositoriesOptionInteractorInputProtocol?
    private let router: RepositoriesOptionWireframeProtocol

    init(interface: RepositoriesOptionViewProtocol, interactor: RepositoriesOptionInteractorInputProtocol?, router: RepositoriesOptionWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    
    func fetchData(){
        self.view?.showActivityIndicator()
        self.interactor?.fetchData()
    }
  
    func getRepositoriesOptionList() -> [RepositoriesOptionModel] {
        var optionArray = [RepositoriesOptionModel]()
        guard let data = self.interactor?.getRepositoriesData() else {
            return optionArray
        }
        if let languageUrl = data.languages {
            let language = RepositoriesOptionModel.init(name: RepositoriesOption.Launguage.title,fullName:data.fullName, url: languageUrl, type: RepositoriesOption.Launguage)
            optionArray.append(language)
        }
        if let branchesUrl = data.branches {
            let branches = RepositoriesOptionModel.init(name: RepositoriesOption.Branch.title,fullName:data.fullName, url: branchesUrl, type: RepositoriesOption.Branch)
            optionArray.append(branches)
        }
        if let contributorsUrl = data.contributors {
            let contributors = RepositoriesOptionModel.init(name: RepositoriesOption.Contributor.title,fullName:data.fullName, url: contributorsUrl, type: RepositoriesOption.Contributor )
            optionArray.append(contributors)
        }
        return optionArray
    }
    func showOptionDetails(atIndex:Int) {
        let data = getRepositoriesOptionList()[atIndex]
        if data.url != nil && data.type == RepositoriesOption.Launguage{
            
            self.router.navigateToLanguageScreen(with: data)
        }
        if data.url != nil && data.type == RepositoriesOption.Branch{
            
            self.router.navigateToBranchScreen(with: data)
        }
        if data.url != nil && data.type == RepositoriesOption.Contributor{
            self.router.navigateToContributorsScreen(with: data)
        }
      
    }
    func getOptionsCount() -> Int {
        return self.getRepositoriesOptionList().count
    }
    
}
extension RepositoriesOptionPresenter:RepositoriesOptionInteractorOutputProtocol {
    
    func onDataAvailable() {
        self.view?.hideActivityIndicator()
        self.view?.onDataAvailable()
    }
    func onDataNotAvailable(){
        self.view?.hideActivityIndicator()
        self.view?.onDataNotAvailable()

    }
}
