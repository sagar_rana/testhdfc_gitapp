//
//  RepositoriesListProtocols.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//


import UIKit

//MARK: Wireframe -
protocol RepositoriesListWireframeProtocol: class {
    var viewController: UIViewController? { get set }

    
    static func createModule() -> UIViewController
    func navigateToRepositoriesOptionScreen(with data:RepositoriesModel?)

}
//MARK: Presenter -
protocol RepositoriesListPresenterProtocol: class {
    var interactor: RepositoriesListInteractorInputProtocol! { get set }
    func fetchRepositories()
    func getRepositoriesCount() -> Int
    func getRepositoriesData(atIndex:Int) -> RepositoriesModel
    func showOptions(atIndex:Int)

}

//MARK: Interactor -
protocol RepositoriesListInteractorOutputProtocol: class {

    func onRepositoriesRetrieved()
    func onError(error:NetworkErrors?)
}

protocol RepositoriesListInteractorInputProtocol: class {

    var presenter: RepositoriesListInteractorOutputProtocol?  { get set }
    func retrieveRepositoriesList()
    func getRepositoriesList() -> [RepositoriesModel]

}

//MARK: View -
protocol RepositoriesListViewProtocol: IndicatableView {

    var presenter: RepositoriesListPresenterProtocol!  { get set }
    func onRepositoriesRetrieved()
    
}


