//
//  RepositoriesOptionProtocols.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//
import Foundation

//MARK: Wireframe -
protocol RepositoriesOptionWireframeProtocol: class {

    func navigateToLanguageScreen(with data:RepositoriesOptionModel?)
    func navigateToBranchScreen(with data:RepositoriesOptionModel?)
    func navigateToContributorsScreen(with data:RepositoriesOptionModel?)
}
//MARK: Presenter -
protocol RepositoriesOptionPresenterProtocol: class {

    var interactor: RepositoriesOptionInteractorInputProtocol? { get set }
    func fetchData()
    func getRepositoriesOptionList() -> [RepositoriesOptionModel]
    func getOptionsCount() -> Int
    func showOptionDetails(atIndex:Int)
}

//MARK: Interactor -
protocol RepositoriesOptionInteractorOutputProtocol: class {

    func onDataAvailable()
    func onDataNotAvailable()
}

protocol RepositoriesOptionInteractorInputProtocol: class {

   var presenter: RepositoriesOptionInteractorOutputProtocol?  { get set }
   func fetchData()
   func getRepositoriesData() -> RepositoriesModel?
}

//MARK: View -
protocol RepositoriesOptionViewProtocol: IndicatableView {

    var presenter: RepositoriesOptionPresenterProtocol! { get set }
    func onDataAvailable()
    func onDataNotAvailable()

}
