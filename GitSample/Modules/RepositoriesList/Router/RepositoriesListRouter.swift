//
//  RepositoriesListRouter.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class RepositoriesListRouter: RepositoriesListWireframeProtocol {

    weak var viewController: UIViewController?

    static func createModule() -> UIViewController {
        
        let view = StoryBoard.GitSampleMain.instantiateViewController(withIdentifier: RepositoriesListViewController.className) as! RepositoriesListViewController
        let interactor = RepositoriesListInteractor()
        let router = RepositoriesListRouter()
        let presenter = RepositoriesListPresenter(interface: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    func navigateToRepositoriesOptionScreen(with data:RepositoriesModel?) {
        let optionVC = RepositoriesOptionRouter.createModule(with: data)
        viewController?.navigationController?.pushViewController(optionVC, animated: true)
       
    }

}

