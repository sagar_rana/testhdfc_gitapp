//
//  RepositoriesOptionRouter.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class RepositoriesOptionRouter: RepositoriesOptionWireframeProtocol {

    weak var viewController: UIViewController?

    static func createModule(with data:RepositoriesModel?) -> UIViewController {
        let view = StoryBoard.GitSampleMain.instantiateViewController(withIdentifier: RepositoriesOptionsViewController.className) as! RepositoriesOptionsViewController

        let interactor = RepositoriesOptionInteractor()
        let router = RepositoriesOptionRouter()
        let presenter = RepositoriesOptionPresenter(interface: view, interactor: interactor, router: router)

        view.presenter = presenter
        interactor.presenter = presenter
        interactor.data = data
        router.viewController = view

        return view
    }
    func navigateToLanguageScreen(with data:RepositoriesOptionModel?) {
        let languageVC = LanguagesListRouter.createModule(for: data)
        viewController?.navigationController?.pushViewController(languageVC, animated: true)
        
    }
    func navigateToBranchScreen(with data:RepositoriesOptionModel?){
        let languageVC = BranchesListRouter.createModule(for: data)
        viewController?.navigationController?.pushViewController(languageVC, animated: true)
    }
    func navigateToContributorsScreen(with data:RepositoriesOptionModel?){
        let contributorsVC = ContributorsListRouter.createModule(for: data)
        viewController?.navigationController?.pushViewController(contributorsVC, animated: true)
    }
}
