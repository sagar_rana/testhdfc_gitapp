//
//  RepositoriesAPIServiceManager.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class RepositoriesAPIServiceManager:BaseAPIService {
   


    func retrieveRepositoriesList(apiResponseHandler: @escaping([RepositoriesModel]?, NetworkErrors?)->Void) -> Void  {
        
        let urlString = self.apiConfig?.url(path: Endpoints.Repositories.fetch.path)
        let httpHeaders = self.apiConfig?.header()
        Alamofire
            .request(urlString!, method: .get, parameters:  Endpoints.Repositories.fetch.params, headers:httpHeaders)
            .validate()
            .responseArray(completionHandler: { (response: DataResponse<[RepositoriesModel]>) in
                self.debugHTTPHeader(request: response.request! as NSURLRequest)
               
                switch response.result {
                case .success(let repositories):
                    
                    apiResponseHandler(repositories,nil)
                    
                case .failure(let error):
                     let errorInfo = self.error(withError: error, withData: response.data, responseCode: (response.response?.statusCode))
                     apiResponseHandler(nil,errorInfo)

                }
            })
    }
    
    
    
    
}
