//
//  RepositoriesOptionsViewController.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit
import Material
class RepositoriesOptionsViewController: BaseViewController {
    @IBOutlet weak var tableView: TableView!
    var presenter: RepositoriesOptionPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViewComponents()
        self.presenter.fetchData()
    }
    class var className: String {
        return String(describing: self)
    }
    func setUpViewComponents(){
        self.prepareNavigationItem()
        self.prepareTableView()
    }
    
   
}
extension RepositoriesOptionsViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.getOptionsCount()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GenericTitleTableViewCell.reuseIdentifier, for: indexPath) as! GenericTitleTableViewCell
        let data  = self.presenter.getRepositoriesOptionList()[indexPath.row]
        cell.setTitle(title:data.name)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.showOptionDetails(atIndex:indexPath.row)
    }
}


extension RepositoriesOptionsViewController {
    func prepareNavigationItem(){
        self.setnavigationTitle(title: Screens.Repositories.RepositoriesOption.title)
    }
    func prepareTableView(){
        tableView.estimatedRowHeight = ViewSizes.TableViewCell.SingleLineData.estimatedRowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = nil
    }
  
}
extension RepositoriesOptionsViewController:RepositoriesOptionViewProtocol {
    
    func onDataAvailable(){
        self.tableView.reloadData()
    }
    func onDataNotAvailable(){
        
    }
    
}


