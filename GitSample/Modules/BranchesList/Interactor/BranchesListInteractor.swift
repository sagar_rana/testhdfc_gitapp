//
//  BranchesListInteractor.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class BranchesListInteractor: BranchesListInteractorInputProtocol {
   
    
    var remoteService = BranchAPIServiceManager()
    weak var presenter: BranchesListInteractorOutputProtocol?
    var option:RepositoriesOptionModel?
    var branchesList :[BranchModel]?

 
    func retrieveBranchList() {
        remoteService.retrieveBranchesList(userPath:option?.fullName){[weak self] (data, error) in
            guard let strongSelf = self else { return }
            if let branchesList = data {
                strongSelf.branchesList = branchesList
                strongSelf.presenter?.onBranchRetrieved()
            }
            else {
                strongSelf.presenter?.onError(error: error)
            }
        }
    }
    
    func getBranchList() -> [BranchModel] {
        var branchList = [BranchModel]()
        if let data = self.branchesList {
            branchList = data
        }
        return branchList
    }
}
