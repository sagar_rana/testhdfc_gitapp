//
//  BranchesListProtocols.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation

//MARK: Wireframe -
protocol BranchesListWireframeProtocol: class {

}
//MARK: Presenter -
protocol BranchesListPresenterProtocol: class {
    var interactor: BranchesListInteractorInputProtocol! { get set }
    func fetchBranches()
    func getBranchCount() -> Int
    func getBranch(atIndex:Int) -> BranchModel
}

//MARK: Interactor -
protocol BranchesListInteractorOutputProtocol: class {

    func onBranchRetrieved()
    func onError(error:NetworkErrors?)
}

protocol BranchesListInteractorInputProtocol: class {

    var presenter: BranchesListInteractorOutputProtocol?  { get set }
    func retrieveBranchList()
    func getBranchList() -> [BranchModel]
}

//MARK: View -
protocol BranchesListViewProtocol: IndicatableView {

    var presenter: BranchesListPresenterProtocol!  { get set }
    func onBranchesRetrieved()

}
