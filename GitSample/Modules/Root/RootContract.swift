//
//  RootContract.swift
//  GitSample
//
//  Created by Sagar Rana on 12/05/18.
//  Copyright © 2018 hs. All rights reserved.
//

import UIKit

protocol RootWireframe: class {
    func presentRepositoriesScreen(in window: UIWindow)
}
