//
//  BaseViewController.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
import UIKit
import Material
import MaterialActivityIndicator

class BaseViewController: UIViewController {
    private let indicator = MaterialActivityIndicatorView()

    override func viewDidLoad() {
        // Do any additional setup after loading the view.
        super.viewDidLoad()
        setupActivityIndicatorView()

   
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setnavigationTitle(title:String?){
        guard  let title = title else {
            return
        }
        self.navigationItem.titleLabel.text = title
    }
    
  
}
// Activity code
extension BaseViewController {
    func startActivityIndicator(){
        indicator.startAnimating()
    }
    func stopActivityIndicator(){
        indicator.stopAnimating()

    }
    private func setupActivityIndicatorView() {
        view.addSubview(indicator)
        indicator.color = Color.indigo.base
        setupActivityIndicatorViewConstraints()
    }
    
    private func setupActivityIndicatorViewConstraints() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}
// Generic Alert
extension BaseViewController {
    func showAlertController(title: String?, message: String?) {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        alert.message = message
        
        let okActionButton = UIAlertAction(title: "ok", style: .cancel) { action -> Void in
          
        }
        alert.addAction(okActionButton)
        self.present(alert, animated: true) {
            
        }
    }
}
