//
//  LanguagesListRouter.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class LanguagesListRouter: LanguagesListWireframeProtocol {

    weak var viewController: UIViewController?

    static func createModule(for option:RepositoriesOptionModel?) -> UIViewController {
        let view = StoryBoard.GitSampleMain.instantiateViewController(withIdentifier: LanguagesListViewController.className) as! LanguagesListViewController
        let interactor = LanguagesListInteractor()
        let router = LanguagesListRouter()
        let presenter = LanguagesListPresenter(interface: view, interactor: interactor, router: router)

        view.presenter = presenter
        interactor.presenter = presenter
        interactor.option = option
        router.viewController = view

        return view
    }
}
