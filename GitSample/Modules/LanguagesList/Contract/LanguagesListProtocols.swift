//
//  LanguagesListProtocols.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation

//MARK: Wireframe -
protocol LanguagesListWireframeProtocol: class {

}
//MARK: Presenter -
protocol LanguagesListPresenterProtocol: class {

    var interactor: LanguagesListInteractorInputProtocol! { get set }
    func fetchLanguages()
    func getlanguageCount() -> Int
    func getlanguage(atIndex:Int) -> String?
}

//MARK: Interactor -
protocol LanguagesListInteractorOutputProtocol: class {

    /* Interactor -> Presenter */
    func onLanguageRetrieved()
    func onError(error:NetworkErrors?)
}

protocol LanguagesListInteractorInputProtocol: class {

    var presenter: LanguagesListInteractorOutputProtocol?  { get set }
    func retrieveLanguageList()
    func getLanguagesList() -> [String]
    /* Presenter -> Interactor */
}

//MARK: View -
protocol LanguagesListViewProtocol: IndicatableView {

    var presenter: LanguagesListPresenterProtocol!  { get set }
    func onLanguageRetrieved()
    func onError(error:NetworkErrors?)
    /* Presenter -> ViewController */
}
