//
//  LanguagesListViewController.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit
import Material
class LanguagesListViewController: BaseViewController {

	var presenter: LanguagesListPresenterProtocol!

    @IBOutlet weak var tableView: TableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViewComponents()
        self.presenter.fetchLanguages()
    }
    class var className: String {
        return String(describing: self)
    }
    func setUpViewComponents(){
        self.prepareNavigationItem()
        self.prepareTableView()
    }
	
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title , message: nil, preferredStyle: .alert)
        alert.message = message
        
        let okActionButton = UIAlertAction(title: "ok", style: .cancel) { action -> Void in
            print("Ok")
        }
        alert.addAction(okActionButton)
        self.present(alert, animated: true) {
            
        }
    }


}

extension LanguagesListViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.getlanguageCount()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GenericTitleTableViewCell.reuseIdentifier, for: indexPath) as! GenericTitleTableViewCell
        let title  = self.presenter.getlanguage(atIndex: indexPath.row)
        cell.setTitle(title:title)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension LanguagesListViewController {
    func prepareNavigationItem(){
        self.setnavigationTitle(title: Screens.Languages.LanguagesList.title)
    }
    func prepareTableView(){
        tableView.estimatedRowHeight = ViewSizes.TableViewCell.SingleLineData.estimatedRowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = nil
        
    }
  
}


extension LanguagesListViewController: LanguagesListViewProtocol {
    
    
    func onLanguageRetrieved() {
       self.tableView.reloadData()
    }
    
    func onError(error: NetworkErrors?) {
        
    }
    
    
    
}
