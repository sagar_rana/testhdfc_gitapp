//
//  LanguagesListInteractor.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class LanguagesListInteractor: LanguagesListInteractorInputProtocol {

    var remoteService = LanguagesAPIServiceManager()
    weak var presenter: LanguagesListInteractorOutputProtocol?
    var option:RepositoriesOptionModel?
    var language :LanguageModel?
    func retrieveLanguageList() {
        
        remoteService.retrieveLanguagesList(userPath:option?.fullName){[weak self] (data, error) in
            guard let strongSelf = self else { return }
            if let language = data {
                strongSelf.language = language
                strongSelf.presenter?.onLanguageRetrieved()
            }
            else {
             strongSelf.presenter?.onError(error: error)
            }
        }
    }
    
    func getLanguagesList() -> [String] {
        
        var languageList = [String]()
        guard let language = self.language else {
            return languageList
        }
        
        if let list = language.languageName {
            languageList = list
        }
       
        return languageList
    }
}
