//
//  ContributorsListInteractor.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class ContributorsListInteractor: ContributorsListInteractorInputProtocol {

    var remoteService = ContributorsAPIServiceManager()
    weak var presenter: ContributorsListInteractorOutputProtocol?
    var option:RepositoriesOptionModel?

    var contributorList :[ContributorModel]?
    func retrieveContributorsList() {
        remoteService.retrieveContributorsList(userPath:option?.fullName){[weak self] (data, error) in
            guard let strongSelf = self else { return }
            if let contributorList = data {
                strongSelf.contributorList = contributorList
                strongSelf.presenter?.onContributorsRetrieved()
            }
            else {
                strongSelf.presenter?.onError(error: error)
            }
        }
    }
    func getContributorsList() -> [ContributorModel] {
        var contributorsList = [ContributorModel]()
        if let data = self.contributorList {
            contributorsList = data
        }
        return contributorsList
    }
}
