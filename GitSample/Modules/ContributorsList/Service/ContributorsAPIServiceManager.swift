//
//  ContributorsAPIServiceManager.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation

import Alamofire
import AlamofireObjectMapper

class ContributorsAPIServiceManager:BaseAPIService {
    
    
    
    func retrieveContributorsList(userPath:String?,apiResponseHandler: @escaping([ContributorModel]?, NetworkErrors?)->Void) -> Void  {
        
        guard let urlString = self.apiConfig?.url(scope: Endpoints.contributors.scope.path, userPath: userPath, module: Endpoints.contributors.fetch.path) else {
            return
        }
        let httpHeaders = self.apiConfig?.header()
        Alamofire
            .request(urlString, method: .get, headers:httpHeaders)
            .validate()
            .responseArray(completionHandler: { (response: DataResponse<[ContributorModel]>) in
                self.debugHTTPHeader(request: response.request! as NSURLRequest)
                switch response.result {
                case .success(let repositories):
                    
                    apiResponseHandler(repositories,nil)
                    
                case .failure(let error):
                    let errorInfo = self.error(withError: error, withData: response.data, responseCode: (response.response?.statusCode))
                    apiResponseHandler(nil,errorInfo)
                    
                }
            })
            
        
    }
    
    
    
    
}
