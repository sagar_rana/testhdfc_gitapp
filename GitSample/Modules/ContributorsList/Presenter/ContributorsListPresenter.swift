//
//  ContributorsListPresenter.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class ContributorsListPresenter: ContributorsListPresenterProtocol {

    weak private var view: ContributorsListViewProtocol?
    var interactor: ContributorsListInteractorInputProtocol!
    private let router: ContributorsListWireframeProtocol

    init(interface: ContributorsListViewProtocol, interactor: ContributorsListInteractorInputProtocol?, router: ContributorsListWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    func fetchContributors(){
        self.view?.showActivityIndicator()
        self.interactor?.retrieveContributorsList()
    }
    
    func getContributorsList() -> [ContributorModel] {
        return self.interactor.getContributorsList()
    }
    func getContributorCount() -> Int {
        return self.getContributorsList().count
    }

    func getContributor(atIndex:Int) -> ContributorModel {
        return self.getContributorsList()[atIndex]
    }

}
extension ContributorsListPresenter: ContributorsListInteractorOutputProtocol {
    func onContributorsRetrieved() {
         self.view?.hideActivityIndicator()
        self.view?.onContributorsRetrieved()
    }
    func onError(error: NetworkErrors?) {
        self.view?.hideActivityIndicator()
        self.view?.showAlert(title: ErrorAlerts.Default.DataFetch.title , message: error?.description)
    }
}
