//
//  ContributorsListViewController.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit
import Material
class ContributorsListViewController: BaseViewController {
    var presenter: ContributorsListPresenterProtocol!
    
    @IBOutlet weak var tableView: TableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViewComponents()
        self.presenter.fetchContributors()
    }
    class var className: String {
        return String(describing: self)
    }
    func setUpViewComponents(){
        self.prepareNavigationItem()
        self.prepareTableView()
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title , message: nil, preferredStyle: .alert)
        alert.message = message
        
        let okActionButton = UIAlertAction(title: "ok", style: .cancel) { action -> Void in
            print("Ok")
        }
        alert.addAction(okActionButton)
        self.present(alert, animated: true) {
            
        }
    }
    
}

extension ContributorsListViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.getContributorCount()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContributorTableViewCell.reuseIdentifier, for: indexPath) as! ContributorTableViewCell
        let data  = self.presenter.getContributor(atIndex: indexPath.row)
        cell.setName(info: data.login)
        cell.setImage(imageURL: data.avatarUrl)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}


extension ContributorsListViewController {
    func prepareNavigationItem(){
        self.setnavigationTitle(title: Screens.Contributions.ContributionsList.title)
    }
    func prepareTableView(){
        tableView.estimatedRowHeight = ViewSizes.TableViewCell.SingleLineDataWithAvtarImage.estimatedRowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = nil
        
    }
  
}


extension ContributorsListViewController: ContributorsListViewProtocol {
    
    func onContributorsRetrieved() {
        self.tableView.reloadData()
    }

    func onError(error: NetworkErrors?) {}
 
}
