//
//  ContributorTableViewCell.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit
import Material
import Motion
class ContributorTableViewCell: TableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgAvtar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        prepareTitleLable()
    }
    func prepareTitleLable(){
        lblTitle.font = RobotoFont.regular(with: 16)

    }
    open override func prepare() {
        super.prepare()
        self.dividerColor = Color.grey.lighten2

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setName(info:String?) {
        if let info = info {
            self.lblTitle.text = info.firstUppercased
        }
    }
    func setImage(imageURL:String?) {
        if let unwrappedImageURL = imageURL {
            self.imgAvtar.setImageRoundImage(fromURL: unwrappedImageURL, placeholderImage: PlaceHolders.Default.Avtar.image)
        }
    }
    
}
