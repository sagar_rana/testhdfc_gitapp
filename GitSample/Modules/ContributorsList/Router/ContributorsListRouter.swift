//
//  ContributorsListRouter.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class ContributorsListRouter: ContributorsListWireframeProtocol {

    weak var viewController: UIViewController?

    static func createModule(for option:RepositoriesOptionModel?) -> UIViewController {
        let view = StoryBoard.GitSampleMain.instantiateViewController(withIdentifier: ContributorsListViewController.className) as! ContributorsListViewController
        let interactor = ContributorsListInteractor()
        let router = ContributorsListRouter()
        let presenter = ContributorsListPresenter(interface: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        interactor.option  = option
        router.viewController = view

        return view
    }
}
