//
//  ContributorsListProtocols.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation

//MARK: Wireframe -
protocol ContributorsListWireframeProtocol: class {

}
//MARK: Presenter -
protocol ContributorsListPresenterProtocol: class {

    var interactor: ContributorsListInteractorInputProtocol! { get set }
    func fetchContributors()
    func getContributorCount() -> Int

    func getContributor(atIndex:Int) -> ContributorModel 
}

//MARK: Interactor -
protocol ContributorsListInteractorOutputProtocol: class {

    func onContributorsRetrieved()
    func onError(error:NetworkErrors?)
}

protocol ContributorsListInteractorInputProtocol: class {

    var presenter: ContributorsListInteractorOutputProtocol?  { get set }
    func retrieveContributorsList()
    func getContributorsList() -> [ContributorModel]
}

//MARK: View -
protocol ContributorsListViewProtocol: IndicatableView {

    var presenter: ContributorsListPresenterProtocol!  { get set }
    func onContributorsRetrieved() 
}
