//
//  BaseAPIService.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation

class BaseAPIService {
    var apiConfig:BaseAPIConfig?
    init() {
        self.apiConfig = BaseAPIConfig()
    }

    func error(withError error:Error, withData data:Data?,responseCode code:Int?) -> NetworkErrors {
        let errorInfo = NetworkErrors(error: error, data: data, responseCode: code)
        return errorInfo
    }
    
    func debugHTTPHeader(request: NSURLRequest) {
        print("request.url?.absoluteString: \(request.url?.absoluteString ?? "")")
        let data = request.httpBody
        if (data != nil) {
            print("HTTPBody: " + "\(String(describing: NSString(data: data!, encoding: String.Encoding.utf8.rawValue)))")
        } else {
            print("Error: HTTPBody data nil")
        }
    }
}






