//
//  NetworkErrors.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation

struct NetworkErrors {
    
    let defaultErrorDescription = ErrorAlerts.Default.DataFetch.message
    var description:String!
    var errorCode:String! = ""
    var responseCode:Int?
    var isNetworkAvailable:Bool = true
    
    init(error:Error, data:Data?, responseCode:Int?) {
        self.setErrorDescription(description: self.defaultErrorDescription)
        self.handleError(error: error)
        self.handleErrorData(data: data)
    }
    
    mutating func handleErrorData(data:Data?) {
        if let data = data {
            do{
                if let jsonData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject] {
                    print("API Error JSON:",jsonData)
                    // need to API specific error code

                }
            } catch {
                //print("error getting json error string: \(error)")
            }
        }
    }
    
    func isDefaultErrorMessage() -> Bool{
        if(self.description ==  self.defaultErrorDescription) {
            return true
        }
        return false
    }
    
    private mutating func handleError(error:Error?) {
        if let err = error as? URLError, err.code  == URLError.Code.notConnectedToInternet {
            self.isNetworkAvailable = false
            let errorDescription = ErrorAlerts.Default.Network.message
            self.setErrorDescription(description: errorDescription)
        }
        else {
            self.isNetworkAvailable = true
        }
    }
    
  
    private mutating func setErrorDescription(description:String?) {
        if let unWrappedDescription = description {
            self.description = unWrappedDescription
        }
    }
    
    private mutating func setErrorCode(errorCode:String?) {
        if let unWrappedErrorCode = errorCode {
            self.errorCode = unWrappedErrorCode
        }
    }
}
