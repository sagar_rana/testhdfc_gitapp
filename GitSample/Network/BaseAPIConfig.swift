//
//  BaseAPIConfig.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation

class BaseAPIConfig {
    init() {
 
    }
    
    func url(path:String?) -> String {
        guard let path = path else {
            return ""
        }
        let urlComponents = NSURLComponents()
        urlComponents.scheme = API.scheme
        urlComponents.host = API.host
        urlComponents.path = path
        return (urlComponents.url?.absoluteString)!
    }
    func url(scope:String? = nil,userPath:String? = nil,module:String? = nil) -> String {
        var path = ""
        if let scope = scope  {
            path.append(scope)

        }
        if let userPath = userPath  {
            path.append("/")
            path.append(userPath)
            
        }
        if let module = module  {
            path.append(module)
            
        }

        let urlComponents = NSURLComponents()
        urlComponents.scheme = API.scheme
        urlComponents.host = API.host
        urlComponents.path = path
        return (urlComponents.url?.absoluteString)!
    }
    
    func header() -> [String:String] {
        var headers = [String:String]()
        headers["user-agent"] =  "sagar-rana"
        return headers
    }
    
}
